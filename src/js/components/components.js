/* eslint-disable consistent-return */
import domLoadComponents from './dom-load';
import windowLoadComponents from './window-load';
import unsupportedBrowsers from '../lib/unsupported-browsers';

(() => {
  if (unsupportedBrowsers()) return false;

  // The DOMContentLoaded event fires when parsing of the current page is complete. DOMContentLoaded is a great event to use to hookup UI functionality to complex web pages.
  document.addEventListener('DOMContentLoaded', domLoadComponents, false);

  // The load event fires when all files have finished loading from all resources, including ads and images.
  window.addEventListener('load', windowLoadComponents, false);
})();
