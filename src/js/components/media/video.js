/* eslint-disable no-nested-ternary */
import Component from '../component';
import { getSecondsMinutesFormat, mediaEvents } from './media';

export default class VideoPlayer extends Component {
  constructor($element, options) {
    super($element);

    this.$media = this.target.querySelector('video');
    this.options = Object(options);
    this.prefix = this.options.prefix || 'media';
    this.lang = Object(this.options.lang);
    // lang.currentTime
    // lang.play
    // lang.pause
    // lang.minutes
    // lang.seconds
    // lang.leaveFullscreen
    // lang.enterFullscreen

    this.paused = null;
    this.currentTime = null;
    this.duration = null;
    this.interval = null;

    this.$document = this.target.ownerDocument;
    this.$controls = this.target.querySelector('.media-player-controls');
    // play/pause toggle
    this.$play = this.$controls.querySelector('[data-player-control]');
    this.$playSymbol = this.$play.querySelector('.media-play-symbol');
    this.$pauseSymbol = this.$play.querySelector('.media-pause-symbol');
    // time slider
    this.$time = this.$controls.querySelector('[data-video-time-progress]');
    this.$timeMeter = this.$time.querySelector('.media-meter.media-time-meter');
    this.$timeRange = this.$time.querySelector('.media-range.media-time-range');

    // current time text
    this.$currentTimeText = this.$document.createTextNode('');
    this.$currentTime = this.$controls.querySelector('[data-video-time-left]');
    this.$currentTime.appendChild(this.$currentTimeText);

    // duration time text
    this.durationTime = this.$media.duration;
    this.$durationTime = this.$controls.querySelector('[data-video-time-duration]');
    this.$durationTime.innerText = getSecondsMinutesFormat(this.durationTime);
    this.$durationTime.setAttribute(
      'title',
      `${VideoPlayer.timeToAural(
        this.durationTime,
        this.lang.minutes || 'minutes',
        this.lang.seconds || 'seconds',
      )}`,
    );

    // fullscreen button
    this.$fullscreen = this.$controls.querySelector(
      '.fullscreen-toggle',
    );
    this.$enterFullscreenSymbol = this.$fullscreen.querySelector(
      '.media-enterFullscreen-symbol',
    );
    this.$leaveFullscreenSymbol = this.$fullscreen.querySelector(
      '.media-leaveFullscreen-symbol',
    );

    // fullscreen api
    this._fullscreenchange = 'onfullscreenchange' in this.target
      ? 'fullscreenchange'
      : 'onwebkitfullscreenchange' in this.target
        ? 'webkitfullscreenchange'
        : 'onMSFullscreenChange' in this.target
          ? 'MSFullscreenChange'
          : 'fullscreenchange';

    this._requestFullscreen = this.target.requestFullscreen
    || this.target.webkitRequestFullscreen
    // || this.target.mozRequestFullScreen
    || this.target.msRequestFullscreen;

    this.onCanPlayStart = this.onCanPlayStart.bind(this);
    this.onLoadStart = this.onLoadStart.bind(this);
    this.onLoadedData = this.onLoadedData.bind(this);
    this.onPlayChange = this.onPlayChange.bind(this);
    this.onTimeChange = this.onTimeChange.bind(this);
    this.onPlayClick = this.onPlayClick.bind(this);
    this.onTimeKeydown = this.onTimeKeydown.bind(this);
    this.onTimeClick = this.onTimeClick.bind(this);
    this.onTimeKeydown = this.onTimeKeydown.bind(this);
    this.onFullscreenClick = this.onFullscreenClick.bind(this);
    this.onFullscreenChange = this.onFullscreenChange.bind(this);

    // add event listeners
    this.$media.addEventListener('canplaythrough', this.onCanPlayStart);
    this.$media.addEventListener('loadstart', this.onLoadStart);
    this.$media.addEventListener('loadeddata', this.onLoadedData);
    this.$media.addEventListener('pause', this.onPlayChange);
    this.$media.addEventListener('play', this.onPlayChange);
    this.$media.addEventListener('timeupdate', this.onTimeChange);
    this.$controls.addEventListener('click', this.onPlayClick, false);
    // this.$play.addEventListener('click', this.onPlayClick, false);
    this.$play.addEventListener('keydown', this.onTimeKeydown);
    this.$time.addEventListener('click', this.onTimeClick, false);
    this.$time.addEventListener('keydown', this.onTimeKeydown);
    this.$fullscreen.addEventListener('click', this.onFullscreenClick, false);
    // listen for fullscreen changes
    document.addEventListener(
      this._fullscreenchange,
      this.onFullscreenChange,
    );

    // pointer events from time control
    VideoPlayer.onDrag(this.$time, this.$timeRange, (percentage) => {
      this.$media.currentTime = this.duration * Math.max(0, Math.min(1, percentage));

      this.onTimeChange();
    });

    this.onLoadStart();
  }

  static _fullscreenElement() {
    return document.fullscreenElement
    || document.webkitFullscreenElement
    // || document.mozFullScreenElement
    || document.msFullscreenElement;
  }

  static _exitFullscreen() {
    return document.exitFullscreen
    || document.webkitCancelFullScreen
    // || document.mozCancelFullScreen
    || document.msExitFullscreen;
  }

  // when the play state changes
  onPlayChange() {
    if (this.paused !== this.$media.paused) {
      this.paused = this.$media.paused;

      this.$play.setAttribute(
        'aria-label',
        this.paused ? this.lang.play || 'play' : this.lang.pause || 'pause',
      );
      this.target.setAttribute(
        'data-player-state',
        this.paused ? 'paused' : 'playing',
      );
      this.$playSymbol.setAttribute('aria-hidden', !this.paused);
      this.$pauseSymbol.setAttribute('aria-hidden', this.paused);

      clearInterval(this.interval);

      if (!this.paused) {
        // listen for time changes every 30th of a second
        this.interval = setInterval(this.onTimeChange, 34);
      }

      // dispatch new "playchange" event
      this.$media.dispatchEvent(mediaEvents.playchange);
    }
  }

  // when the time changes
  onTimeChange() {
    if (
      this.currentTime !== this.$media.currentTime
      || this.duration !== this.$media.duration
    ) {
      this.currentTime = this.$media.currentTime;
      this.duration = this.$media.duration || 0;

      const currentTimePercentage = this.currentTime / this.duration;
      // const currentTimeCode = VideoPlayer.timeToTimecode(this.currentTime);
      const currentTimeCode = getSecondsMinutesFormat(this.currentTime);
      // const remainingTimeCode = VideoPlayer.timeToTimecode(
      //   this.duration - Math.floor(this.currentTime),
      // );

      if (currentTimeCode !== this.$currentTimeText.nodeValue) {
        this.$currentTimeText.nodeValue = currentTimeCode;

        this.$currentTime.setAttribute(
          'title',
          `${VideoPlayer.timeToAural(
            this.currentTime,
            this.lang.minutes || 'minutes',
            this.lang.seconds || 'seconds',
          )}`,
        );
      }

      // if (remainingTimeCode !== this.$remainingTimeText.nodeValue) {
      //   this.$remainingTimeText.nodeValue = remainingTimeCode;

      //   $(this.$remainingTime, {
      //     title: `${VideoPlayer.timeToAural(
      //       this.duration - this.currentTime,
      //       this.lang.minutes || 'minutes',
      //       this.lang.seconds || 'seconds',
      //     )}`,
      //   });
      // }

      this.$time.setAttribute('aria-valuenow', this.currentTime);
      this.$time.setAttribute('aria-valuemin', 0);
      this.$time.setAttribute('aria-valuemax', this.duration);

      this.$timeMeter.style.width = `${currentTimePercentage * 100}%`;

      // dispatch new "timechange" event
      this.$media.dispatchEvent(mediaEvents.timechange);
    }
  }

  // when media loads for the first time
  onLoadStart() {
    this.$media.removeEventListener('canplaythrough', this.onCanPlayStart);
    this.$media.addEventListener('canplaythrough', this.onCanPlayStart);

    this.onPlayChange();
    this.onFullscreenChange();
    this.onTimeChange();
  }

  // when the immediate current playback position is available
  onLoadedData() {
    this.onTimeChange();
  }

  // when the media can play
  onCanPlayStart() {
    this.$media.removeEventListener('canplaythrough', this.nCanPlayStart);

    // dispatch new "canplaystart" event
    this.$media.dispatchEvent(mediaEvents.canplaystart);


    if (!this.paused || this.$media.autoplay) {
      this.$media.play();
    }
  }

  onFullscreenChange() {
    const isFullscreen = this.target === VideoPlayer._fullscreenElement();

    this.$fullscreen.setAttribute(
      'aria-label',
      isFullscreen
        ? this.lang.leaveFullscreen || 'leave full screen'
        : this.lang.enterFullscreen || 'enter full screen',
    );
    this.$enterFullscreenSymbol.setAttribute('aria-hidden', isFullscreen);
    this.$leaveFullscreenSymbol.setAttribute('aria-hidden', !isFullscreen);
  }

  // when the play control is clicked
  onPlayClick(event) {
    event.preventDefault();
    event.stopPropagation();

    this.$media[this.$media.paused ? 'play' : 'pause']();
  }

  // when the time control
  onTimeClick(event) {
    event.stopPropagation();

    // handle click if clicked without pointer
    if (!event.pointerType && !event.detail) {
      this.onPlayClick(event);
    }
  }

  // click from fullscreen control
  onFullscreenClick(event) {
    event.stopPropagation();

    if (this._requestFullscreen) {
      if (this.target === VideoPlayer._fullscreenElement()) {
        // exit fullscreen
        VideoPlayer._exitFullscreen().call(document);
      } else {
        // enter fullscreen
        this._requestFullscreen.call(this.target);

        // maintain focus in internet explorer
        this.$fullscreen.focus();

        // maintain focus in safari
        setTimeout(() => {
          this.$fullscreen.focus();
        }, 200);
      }
    } else if (this.$media.webkitSupportsFullscreen) {
      // iOS allows fullscreen of the video itself
      if (this.$media.webkitDisplayingFullscreen) {
        // exit ios fullscreen
        this.$media.webkitExitFullscreen();
      } else {
        // enter ios fullscreen
        this.$media.webkitEnterFullscreen();
      }

      this.onFullscreenChange();
    }
  }

  // keydown from play control or current time control
  onTimeKeydown(event) {
    const { keyCode, shiftKey } = event;

    // 37: LEFT, 38: UP, 39: RIGHT, 40: DOWN
    if (keyCode >= 37 && keyCode <= 40) {
      event.preventDefault();

      const offset = keyCode === 37 || keyCode === 39 ? keyCode - 38 : keyCode - 39;

      this.$media.currentTime = Math.max(
        0,
        Math.min(
          this.$media.duration,
          this.$media.currentTime + offset * (shiftKey ? 10 : 1),
        ),
      );

      this.onTimeChange();
    }
  }

  // Handle Drag Ranges
  static onDrag(target, innerTarget, listener) {
    // eslint-disable-line max-params
    const hasPointerEvent = undefined !== target.onpointerup;
    const hasTouchEvent = undefined !== target.ontouchstart;
    const pointerDown = hasPointerEvent
      ? 'pointerdown'
      : hasTouchEvent
        ? 'touchstart'
        : 'mousedown';
    const pointerMove = hasPointerEvent
      ? 'pointermove'
      : hasTouchEvent
        ? 'touchmove'
        : 'mousemove';
    const pointerUp = hasPointerEvent
      ? 'pointerup'
      : hasTouchEvent
        ? 'touchend'
        : 'mouseup';

    // ...
    const axisProp = 'clientX';

    let window;
    let start;
    let end;

    function onpointermove(event) {
      // prevent browser actions on this event
      event.preventDefault();

      // the pointer coordinate
      const position = axisProp in event
        ? event[axisProp]
        : (event.touches && event.touches[0] && event.touches[0][axisProp])
            || 0;

      // the percentage of the pointer along the container
      const percentage = (position - start) / (end - start);

      // call the listener with percentage
      listener(percentage);
    }

    function onpointerdown(event) {
      // window
      window = target.ownerDocument.defaultView;

      // client boundaries
      const rect = innerTarget.getBoundingClientRect();

      // the container start and end coordinates
      start = rect.left;
      end = rect.right;

      onpointermove(event);

      window.addEventListener(pointerMove, onpointermove);
      window.addEventListener(pointerUp, onpointerup);
    }

    function onpointerup() {
      window.removeEventListener(pointerMove, onpointermove);
      window.removeEventListener(pointerUp, onpointerup);
    }

    // on pointer down
    target.addEventListener(pointerDown, onpointerdown);
  }

  // Time To Timecode
  // DEPRECATED (media.util)
  // static timeToTimecode(time) {
  //   return `${`0${Math.floor(time / 60)}`.slice(-2)}:${`0${Math.floor(
  //     time % 60,
  //   )}`.slice(-2)}`;
  // }

  // Time To Aural
  // DEPRECATED (media.util)
  static timeToAural(time, langMinutes, langSeconds) {
    return `${Math.floor(time / 60)} ${langMinutes}, ${Math.floor(
      time % 60,
    )} ${langSeconds}`;
  }
}
