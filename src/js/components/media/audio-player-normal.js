import Component from '../component';

export default class AudioPlayerNormal extends Component {

  constructor($element) {
    super($element);

    this.$playButton = this.target.querySelector('[data-player-control]');
    this.$loadWave = this.target.querySelector('[data-audio-wave]');
    this.$leftTime = this.target.querySelector('[data-audio-time-left]');
    this.$audioDuration = this.target.querySelector('[data-audio-time-duration]');

    this.wavesurfer = WaveSurfer.create({
      container: this.$loadWave,
      waveColor: '#A77977',
      progressColor: '#FCE4E3',
      height: 80,
      responsive: true,
      barWidth: 2,
    });
    this.wavesurfer.load(this.$loadWave.dataset.src);

    this.initWave();
  }

  initWave() {
    this.wavesurfer.on('ready', () => {
      this.$leftTime.textContent = '0:00';
      const duration = this.wavesurfer.getDuration();
      const minutes = Math.floor((duration % 3600) / 60);
      const seconds = (`00${Math.floor(duration % 60)}`).slice(-2);
      this.$audioDuration.textContent = `${minutes}:${seconds}`;

      this.addListenerAfterReady();
    });
  }

  addListenerAfterReady() {
    this.wavesurfer.on('audioprocess', (time) => {
      const minutes = Math.floor((time % 3600) / 60);
      const seconds = (`00${Math.floor(time % 60)}`).slice(-2);
      this.$leftTime.textContent = `${minutes}:${seconds}`;
    });

    this.wavesurfer.on('finish', () => {
      this.$playButton.setAttribute('data-player-control', 'paused');
    });

    this.$playButton.addEventListener('click', () => {
      this.wavesurfer.playPause();

      if (this.$playButton.getAttribute('data-player-control') === 'paused') {
        this.$playButton.setAttribute('data-player-control', 'playing');
      } else {
        this.$playButton.setAttribute('data-player-control', 'paused');
      }

      this.showFeedback();
    });

    // stop audio prehravac kdyz se zapne dalsi slajd
    const $nextSlides = document.querySelectorAll('[data-nav-button]');
    $nextSlides.forEach(($nextSlide) => {
      $nextSlide.addEventListener('click', () => {
        if (this.$playButton.getAttribute('data-player-control') === 'playing') {
          this.wavesurfer.playPause();
          this.$playButton.setAttribute('data-player-control', 'paused');
        }
      });
    });

  }

}
