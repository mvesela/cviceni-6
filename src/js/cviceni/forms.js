import Cviceni from './cviceni';


// javascript pro první a poslední slajd
export default class Forms extends Cviceni {
  constructor() {
    super();

    this.$form = this.$formSlide.querySelector('#form');
    this.$userEmail = this.$form.querySelector('#user-email');
    this.$groupMembers = this.$form.querySelector('#form-group-members');
    this.$modes = this.$form.querySelectorAll('[data-exercise-mode]');
    this.$button = this.$formSlide.querySelector('[data-nav-button="next"]');
    this.$showUserEmail = document.querySelectorAll('[data-form="about-user-email"]');
    this.$formExport = this.$exportSlide.querySelector('#form-to-send');

    this.isFromLMS = ((typeof this.import.lmsHash === 'string') && (this.import.lmsHash.length > 0));
    this.groupMembers = 1;
  }

  init() {
    this.$button.classList.remove('button-hidden');

    // cvičení je v prohlížecím módu
    if (this.isDone) {
      this.whenDone();
    }
    // cvičení je ve vyplňovacím módu
    else {
      // check if we have user in URL and prefill the form
      if (this.urlParams.get('email')) {
        this.$userEmail.value = this.urlParams.get('email');
        this.$userEmail.readOnly = true;
        this.$userEmail.classList.add('is-readonly');
      }

      if (this.isExhibition) {
        // similar use case when from LMS
        this.$formSlide.querySelectorAll('.hide-when-from-lms').forEach(($hideForLms) => {
          $hideForLms.classList.add('hidden');
        });
      }

      this.addEventListenersToExerciseModeRadioButtons();
      this.addGroupMemberField();

      if (this.isFromLMS) {
        this.whenFromLMS();
      }
    }
  }

  whenFromLMS() {
    // schovat prvky nepotřebné při příchodu z LMS
    this.$formSlide.querySelectorAll('.hide-when-from-lms').forEach(($hideForLms) => {
      $hideForLms.classList.add('hidden');
    });

    // pokud skupinová práce
    if (this.import.exerciseMode === 1) {

      // if (this.import.user.length > 0) {
      //   this.$form.classList.add('hidden');

      //   this.$form.querySelector('.hide-when-from-lms-with-user'.forEach(($hideForLmsWithUser) => {
      //     $hideForLmsWithUser.classList.add('hidden');
      //   });

      //   this.$button.innerText = 'Pustit se do cvičení!';
      // }

      // zobraz formulář, protože potřebujeme zobrazit pole pro další členy skupiny
      this.$form.classList.remove('hidden');
      this.$groupMembers.classList.remove('hidden');

    }
    // else if (this.import.user.length > 0) {
    //   this.$form.classList.add('hidden');

    //   this.$form.querySelector('.hide-when-from-lms-with-user'.forEach(($hideForLmsWithUser) => {
    //     $hideForLmsWithUser.classList.add('hidden');
    //   });

    //   this.$button.innerText = 'Pustit se do cvičení!';
    // }
  }

  getAnonymizedEmail() {
    // destructure email
    const emailArray = this.import.user.match(/(.{1})([^@]*)(.{2})([^.]*)(.*)/);

    return emailArray[1] + emailArray[2].replace(/./g, '*') + emailArray[3] + emailArray[4].replace(/./g, '*') + emailArray[5];
  }

  whenDone() {
    // vypsat email uživatele
    this.$showUserEmail.forEach(($userEmail) => {
      if (this.urlParams.get('anon') && this.import.user) {
        this.import.user = this.getAnonymizedEmail();
      }

      $userEmail.innerText += ` ${this.import.user || 'neznámý uživatel'}`;
    });

    // vypsat alternativní copy tlačítka
    this.$button.innerText = this.$button.getAttribute('data-done');

    // schovat formulář z prvního slajdu
    this.$form.classList.add('hidden');
    // schovat a zneaktivnit formulář z posledního slajdu
    this.$formExport.setAttribute('action', '');
    this.$formExport.classList.add('hidden');
  }

  addEventListenersToExerciseModeRadioButtons() {
    this.$modes.forEach(($mode) => {
      $mode.addEventListener('change', () => {
        if ($mode.value === '1') {
          this.$groupMembers.classList.remove('hidden');
        } else {
          this.$groupMembers.classList.add('hidden');
        }
      });
    });
  }

  addGroupMemberField() {
    const $addGroupMember = this.$groupMembers.querySelector('[data-form="group-member-add"]');
    const $masterGroupMember = this.$groupMembers.querySelector('[data-form="group-member-master"]');
    const $parentElement = $addGroupMember.parentElement;

    $addGroupMember.addEventListener('click', (event) => {
      const $groupMember = $masterGroupMember.cloneNode();

      $groupMember.value = '';
      $groupMember.id = $groupMember.id.replace('1', this.groupMembers += 1);
      $groupMember.removeAttribute('data-form');

      $parentElement.insertBefore($groupMember, $addGroupMember);

      $groupMember.focus({ preventScroll: false });

      event.preventDefault();
    });
  }
}
