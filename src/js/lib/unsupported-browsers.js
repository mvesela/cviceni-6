const unsupportedBrowsers = () => {

  // feature detection for unsupported browsers
  const isUnsupported = !Array.from
    || !Element.prototype.matches
    || !Element.prototype.closest
    || typeof window.CustomEvent !== 'function'
    || typeof Object.assign !== 'function';

  if (isUnsupported) {
    console.warn('Not supported browser.');

    const $unsupported = document.querySelector('#unsupported');
    $unsupported.querySelector('#unsupported-browser').innerHTML = ` ${platform.name} ${platform.version} `;
    $unsupported.classList.remove('is-hidden');

    return true;
  }

  return false;
};

export default unsupportedBrowsers;
